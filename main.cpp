
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Custom Exceptions              */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
struct NonexistentElementException: public exception {
    const char* what() const throw() {
        return "A nonexistent or missing element was requested from a linked list or graph.";
    }
};

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Linked List Implementation     */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
template <typename T>
struct ListNode {
    T value;
    ListNode<T>* next;
};

template <typename T>
class List {
    public:

    ListNode<T>* getHead() {
        return head;
    }

    ListNode<T>* getTail() {
        if (head == NULL)
            return NULL;

        ListNode<T>* current = head;

        while (current->next != NULL) {
            current = current->next;
        }

        return current;
    }

    int getLength() {
        if (head == NULL)
            return 0;

        ListNode<T>* current = head;
        int counter = 0;

        while (current != NULL) {
            counter++;
            current = current->next;
        }

        return counter;
    }

    T getItem(int position) {
        if (head == NULL)
            throw NonexistentElementException();

        ListNode<T>* current = head;
        int counter = 0;

        while (counter != position) {
            counter++;
            current = current->next;

            if (current == NULL)
                throw NonexistentElementException();
        }

        return current->value;
    }

    vector<T> getItems() {
        if (head == NULL)
            return vector<T>();

        vector<T> items;

        ListNode<T>* current = head;
        int counter = 0;

        while(current != NULL) {
            counter++;
            items.push_back(current->value);
            current = current->next;
        }

        return items;
    }

    void append(T value) {
        ListNode<T>* newNode = new ListNode<T>;
        newNode->next = NULL;
        newNode->value = value;

        if (head == NULL) {
            head = newNode;
        } else {
            getTail()->next = newNode;
        }
    }

    void prepend(T value) {
        ListNode<T>* newNode = new ListNode<T>;
        newNode->next = head;
        newNode->value = value;

        head = newNode;
    }

    int indexOf(T value) {
        if (head == NULL)
            return -1;

        ListNode<T>* current = head;
        int counter = 0;

        while (current != NULL) {
            if (current->value == value)
                return counter;

            counter++;
            current = current->next;
        }
    }

    void remove(int position) {
        if (head == NULL)
            throw NonexistentElementException();

        if (position == 0) {
            ListNode<T>* oldHead = head;

            head = oldHead->next;
            delete oldHead;

            return;
        }

        ListNode<T>* previous = NULL;
        ListNode<T>* current = head;
        int counter = 0;

        while (counter != position) {
            counter++;
            previous = current;
            current = current->next;

            if (current == NULL)
                throw NonexistentElementException();
        }

        previous->next = current->next;
        delete current;
    }

    bool contains(T value) {
        if (head == NULL)
            return false;

        ListNode<T>* current = head;

        while (current != NULL) {
            if (current == value)
                return true;

            current = current->next;
        }
    }

    private:
    ListNode<T>* head = NULL;
};

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Graph Implementation           */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
struct GraphVertex {
    string name;
};

struct GraphEdge {
    GraphVertex* vertexA;
    GraphVertex* vertexB;
    int weight;
};

class Graph {
    public:

    Graph() {
        vertices = List<GraphVertex*>();
        edges = List<GraphEdge*>();
    }

    void addVertex(string name) {
        GraphVertex* vertex = new GraphVertex;
        vertex->name = normalizeVertexName(name);

        vertices.append(vertex);
    }

    void removeVertex(string name) {
        vertices.remove(vertices.indexOf(getVertex(name)));
        removeInvalidEdges();
    }

    void removeEdge(string vertexAName, string vertexBName) {
        edges.remove(edges.indexOf(getEdge(vertexAName, vertexBName)));
    }

    void addEdge(string vertexAName, string vertexBName, int weight) {
        GraphVertex* vertexA = getVertex(normalizeVertexName(vertexAName));
        GraphVertex* vertexB = getVertex(normalizeVertexName(vertexBName));

        if (vertexA == NULL || vertexB == NULL) {
            throw NonexistentElementException();
        } else if (vertexA->name == vertexB->name) {
            return;
        }

        GraphEdge* edge = new GraphEdge;
        edge->vertexA = vertexA;
        edge->vertexB = vertexB;
        edge->weight = weight;

        edges.append(edge);
    }

    GraphVertex* getVertex(string name) {
        name = normalizeVertexName(name);

        vector<GraphVertex*> currentVertices = getVertices();
        for (int i = 0; i < currentVertices.size(); i++) {
            GraphVertex* vertex = currentVertices[i];

            if (vertex->name == name) {
                return vertex;
            }
        }

        return NULL;
    }

    bool vertexExists(string name) {
        name = normalizeVertexName(name);

        vector<GraphVertex*> currentVertices = getVertices();
        for (int i = 0; i < currentVertices.size(); i++) {
            GraphVertex* vertex = currentVertices[i];

            if (vertex->name == name) {
                return true;
            }
        }

        return false;
    }

    GraphEdge* getEdge(string vertexAName, string vertexBName) {
        vertexAName = normalizeVertexName(vertexAName);
        vertexBName = normalizeVertexName(vertexBName);

        vector<GraphEdge*> currentEdges = getEdges();
        for (int i = 0; i < currentEdges.size(); i++) {
            GraphEdge* edge = currentEdges[i];

            if (
                (edge->vertexA->name == vertexAName && edge->vertexB->name == vertexBName) ||
                (edge->vertexB->name == vertexAName && edge->vertexA->name == vertexBName)
            ) {
                return edge;
            }
        }

        return NULL;
    }

    bool edgeExists(string vertexAName, string vertexBName) {
        vertexAName = normalizeVertexName(vertexAName);
        vertexBName = normalizeVertexName(vertexBName);

        vector<GraphEdge*> currentEdges = getEdges();
        for (int i = 0; i < currentEdges.size(); i++) {
            GraphEdge* edge = currentEdges[i];

            if (
                (edge->vertexA->name == vertexAName && edge->vertexB->name == vertexBName) ||
                (edge->vertexB->name == vertexAName && edge->vertexA->name == vertexBName)
            ) {
                return true;
            }
        }

        return false;
    }

    vector<GraphVertex*> getVertices() {
        return vertices.getItems();
    }

    vector<GraphEdge*> getEdges() {
        return edges.getItems();
    }

    vector<GraphVertex*> getAdjacentVertices(string vertexName) {
        if (!vertexExists(vertexName))
            return vector<GraphVertex*>();

        vector<GraphVertex*> vertices;

        vector<GraphEdge*> currentEdges = getEdges();
        for (int i = 0; i < currentEdges.size(); i++) {
            GraphEdge* edge = currentEdges[i];

            if (edge->vertexA->name == vertexName) {
                vertices.push_back(edge->vertexB);
            } else if(edge->vertexB->name == vertexName) {
                vertices.push_back(edge->vertexA);
            }
        }

        return vertices;
    }

    private:
    List<GraphVertex*> vertices;
    List<GraphEdge*> edges;

    static string normalizeVertexName(string vertexName) {
        string vertexNameCopy = vertexName;

        for (int i = 0; i < vertexNameCopy.size(); i++) {
            char character = vertexNameCopy[i];

            if (character == ' ')
                vertexNameCopy[i] = '_';

            vertexNameCopy[i] = tolower(character);
        }

        return vertexNameCopy;
    }

    void removeInvalidEdges() {
        vector<GraphEdge*> currentEdges = getEdges();
        for (int i = 0; i < currentEdges.size(); i++) {
            GraphEdge* edge = currentEdges[i];

            if (
                !vertexExists(edge->vertexA->name) ||
                !vertexExists(edge->vertexB->name)
            ) {
                removeEdge(edge->vertexA->name, edge->vertexB->name);
            }
        }
    }
};

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* User Interface                 */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
Graph graph = Graph();

string getVertexDisplayName(string vertexName) {
    string vertexNameCopy = vertexName;

    vertexNameCopy[0] = toupper(vertexNameCopy[0]);
    replace(vertexNameCopy.begin(), vertexNameCopy.end(), '_', ' ');

    return vertexNameCopy;
}

vector<string> getGraphPathByDepth(string initialVertex) {
    List<string> vertexNames = List<string>();

    vector<GraphVertex*> vertices = graph.getVertices();
    for (int i = 0; i < vertices.size(); i++) {
        vertexNames.append(vertices[i]->name);
    }

    return vertexNames.getItems();
}

vector<string> getGraphPathByWidth(string initialVertex) {
    List<string> vertexNames = List<string>();

    vector<GraphVertex*> vertices = graph.getVertices();
    for (int i = 0; i < vertices.size(); i++) {
        vertexNames.append(vertices[i]->name);
    }

    return vertexNames.getItems();
}

void displaySeparator() {
#if defined(__linux__)
    cout << "\033[H\033[2J" << endl;
#elif defined(_WIN32) || defined(_WIN64)
    cout << "~~~~~~~~~~~~~~~~~~~~" << endl;
#endif
}

void displayContinuationPrompt() {
    cout << "---" << endl;
    cout << "Introduzca cualquier texto para continuar:" << endl;
    cout << "> ";

    /* TODO: A better implementation requires using platform-specific libraries. */
    string text;
    getline(cin >> ws, text);
}

void displayVertices() {
    displaySeparator();

    cout << "Vertices:" << endl;

    vector<GraphVertex*> currentVertices = graph.getVertices();
    for (int i = 0; i < currentVertices.size(); i++) {
        GraphVertex* vertex = currentVertices[i];
        cout << "- " << getVertexDisplayName(vertex->name) << endl;
    }

    displayContinuationPrompt();
}

void displayEdges() {
    displaySeparator();

    cout << "Aristas:" << endl;

    vector<GraphEdge*> currentEdges = graph.getEdges();
    for (int i = 0; i < currentEdges.size(); i++) {
        GraphEdge* edge = currentEdges[i];
        cout << "- " << getVertexDisplayName(edge->vertexA->name) << " <--" << edge->weight << "--> " << getVertexDisplayName(edge->vertexB->name) << endl;
    }

    displayContinuationPrompt();
}

void displayAddVertex(bool retrying = false) {
    displaySeparator();

    if (retrying)
        cout << "Ya existe un vertice con ese identificador, ingrese otro:" << endl;
    else
        cout << "Introduzca el identificador del vertice:" << endl;
    cout << "> ";

    string vertexName;
    getline(cin >> ws, vertexName);

    if (vertexName == "salir" || vertexName == "exit")
        return;

    if (graph.vertexExists(vertexName)) {
        displayAddVertex(true);
        return;
    }

    graph.addVertex(vertexName);

    displaySeparator();
    cout << "Vertice creado exitosamente." << endl;
    displayContinuationPrompt();
}

void displayAddEdge(bool retrying = false) {
    displaySeparator();

    if (retrying)
        cout << "Uno o ambos vertices no existen, o ya existe una arista para ellos, intentelo de nuevo." << endl;

    string vertexAName;
    string vertexBName;
    string weight;
    int weightNumber;

    cout << "Introduzca el identificador del primer vertice:" << endl;
    cout << "> ";
    getline(cin >> ws, vertexAName);

    if (vertexAName == "salir" || vertexAName == "exit")
        return;

    cout << "Introduzca el identificador del segundo vertice:" << endl;
    cout << "> ";
    getline(cin >> ws, vertexBName);

    if (vertexBName == "salir" || vertexBName == "exit")
        return;

    if (graph.edgeExists(vertexAName, vertexBName) || !graph.vertexExists(vertexAName) || !graph.vertexExists(vertexBName)) {
        displayAddEdge(true);
        return;
    }

    cout << "Introduzca el peso de la arista:" << endl;
    cout << "> ";
    getline(cin >> ws, weight);
    sscanf(weight.c_str(), "%d", &weightNumber);

    graph.addEdge(vertexAName, vertexBName, weightNumber);

    displaySeparator();
    cout << "Arista creada exitosamente." << endl;
    displayContinuationPrompt();
}

void displayRemoveVertex(bool retrying = false) {
    displaySeparator();

    if (retrying)
        cout << "No existe un vertice con ese identificador, ingrese otro:" << endl;
    else
        cout << "Introduzca el identificador del vertice:" << endl;
    cout << "> ";

    string vertexName;
    getline(cin >> ws, vertexName);

    if (vertexName == "salir" || vertexName == "exit")
        return;

    if (!graph.vertexExists(vertexName)) {
        displayRemoveVertex(true);
        return;
    }

    graph.removeVertex(vertexName);

    displaySeparator();
    cout << "Vertice eliminado exitosamente." << endl;
    displayContinuationPrompt();
}

void displayRemoveEdge(bool retrying = false) {
    displaySeparator();

    if (retrying)
        cout << "Uno o ambos vertices no existen, o no existe una arista para ellos, intentelo de nuevo." << endl;

    string vertexAName;
    string vertexBName;

    cout << "Introduzca el identificador del primer vertice:" << endl;
    cout << "> ";
    getline(cin >> ws, vertexAName);

    if (vertexAName == "salir" || vertexAName == "exit")
        return;

    cout << "Introduzca el identificador del segundo vertice:" << endl;
    cout << "> ";
    getline(cin >> ws, vertexBName);

    if (vertexBName == "salir" || vertexBName == "exit")
        return;

    if (!graph.edgeExists(vertexAName, vertexBName)) {
        displayRemoveEdge(true);
        return;
    }

    graph.removeEdge(vertexAName, vertexBName);

    displaySeparator();
    cout << "Arista eliminada exitosamente." << endl;
    displayContinuationPrompt();
}

void displayAdjacencyList() {
    displaySeparator();

    vector<GraphVertex*> vertices = graph.getVertices();
    for (int i = 0; i < vertices.size(); i++) {
        GraphVertex* vertex = vertices[i];

        cout << getVertexDisplayName(vertex->name) << " ->";

        vector<GraphVertex*> adjacentVertices = graph.getAdjacentVertices(vertex->name);
        for (int j = 0; j < adjacentVertices.size(); j++) {
            GraphVertex* adjacentVertex = adjacentVertices[j];
            cout << " [" << getVertexDisplayName(adjacentVertex->name) << "]";
        }

        cout << endl;
    }

    displayContinuationPrompt();
}

void displayGraphPaths(bool retrying = false) {
    displaySeparator();

    if (retrying)
        cout << "No existe un vertice con ese identificador, ingrese otro:" << endl;
    else
        cout << "Introduzca el nombre del vertice de salida:" << endl;
    cout << "> ";

    string vertexName;
    getline(cin >> ws, vertexName);

    if (vertexName == "salir" || vertexName == "exit")
        return;

    if (!graph.vertexExists(vertexName)) {
        displayGraphPaths(true);
        return;
    }

    vector<string> verticesByDepth = getGraphPathByDepth(vertexName);
    vector<string> verticesByWidth = getGraphPathByWidth(vertexName);

    cout << endl << "Primero Profundidad:" << endl;
    for (int i = 0; i < verticesByDepth.size(); i++) {
        cout << getVertexDisplayName(verticesByDepth[i]);

        if (i < verticesByDepth.size() - 1)
            cout << " - ";
    }
    cout << endl;

    cout << endl << "Primero Anchura:" << endl;
    for (int i = 0; i < verticesByWidth.size(); i++) {
        cout << getVertexDisplayName(verticesByWidth[i]);

        if (i < verticesByWidth.size() - 1)
            cout << " - ";
    }
    cout << endl;

    displayContinuationPrompt();
}

void displayMenu() {
    displaySeparator();
    cout << "Introduzca el numero de la opcion deseada:" << endl << endl;
    cout << "1) Mostrar Vertices" << endl;
    cout << "2) Mostrar Aristas" << endl;
    cout << "3) Crear Vertice" << endl;
    cout << "4) Crear Arista" << endl;
    cout << "5) Eliminar Vertice" << endl;
    cout << "6) Eliminar Arista" << endl;
    cout << "7) Mostrar Listas de Adyacencia" << endl;
    cout << "8) Salir" << endl;
    cout << endl << "> ";

    string action;
    int actionNumber;

    cin >> action;

    actionNumber = 0;

    sscanf(action.c_str(), "%d", &actionNumber);

    switch (actionNumber) {
        case 1:
            displayVertices();
            break;
        case 2:
            displayEdges();
            break;
        case 3:
            displayAddVertex();
            break;
        case 4:
            displayAddEdge();
            break;
        case 5:
            displayRemoveVertex();
            break;
        case 6:
            displayRemoveEdge();
            break;
        case 7:
            displayAdjacencyList();
            break;
        case 8:
            cout << "Adios." << endl;
            return;
        default:
            displaySeparator();
            cout << "Accion desconocida." << endl;
            displayContinuationPrompt();
            break;
    }

    displayMenu();
}

void addInitialData() {
    // graph.addVertex("Panama");
    graph.addVertex("Bocas del toro");
    graph.addVertex("Cocle");
    graph.addVertex("Colon");
    graph.addVertex("Darien");
    graph.addVertex("Chiriqui");
    graph.addVertex("Herrera");
    graph.addVertex("Los santos");
    graph.addVertex("Veraguas");

    // graph.addEdge("Panama", "Colon", 1200);
    // graph.addEdge("Panama", "Darien", 1200);
    // graph.addEdge("Panama", "Cocle", 1200);
    graph.addEdge("Bocas del toro", "Chiriqui", 1600);
    graph.addEdge("Chiriqui", "Veraguas", 1300);
    graph.addEdge("Veraguas", "Los Santos", 1100);
    graph.addEdge("Veraguas", "Herrera", 1100);
    graph.addEdge("Veraguas", "Cocle", 1100);
    graph.addEdge("Veraguas", "Colon", 1100);
}

int main() {
    addInitialData();
    displayMenu();
}